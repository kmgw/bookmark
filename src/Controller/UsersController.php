<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    // 問題なし
    const USER_CHECK_STATUS_OK        = 0;
    // bookmark画面に遷移させる
    const USER_CHECK_STATUS_BACKINDEX = 1;


    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['logout', 'login']);

        if ($this->Auth->user('role') === Configure::read('userRole.admin')) {
            // 全てのアクションを許可
            $this->Auth->allow();
        } else if ($this->Auth->user('id')) {
            // ログイン中だけど、管理者以外は一部のアクションのみ許可
            $this->Auth->allow(['display', 'view', 'edit']);
        }

        // ログインユーザーの情報を取得
        $user = $this->Auth->user();        
        // ビューに渡す
        $this->set('auth', $user);
    }

    public function isAuthorized($user)
    {
        $action = $this->request->getParam('action');

        // admin ユーザーだけが一覧、追加、削除画面にアクセス可能
        if (in_array($action, ['index', 'add', 'delete'])) {
            return (bool)($this->Auth->user('role') === Configure::read('userRole.admin'));
        } else {
            return (bool)$this->Auth->user('id');
        }

        // デフォルトでは、アクセスを拒否します。
        return false;
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        // ログインチェック、ユーザーid一致チェック、状況に応じてリダイレクト
        $user_id_check_res = $this->_checkUserId($id);

        if ($user_id_check_res) {
            return;
        }

        try {
            $user = $this->Users->get($id, [
                'contain' => ['Bookmarks']
            ]);
        } catch(\Exception $e) {
            $this->_exeRedirect(self::USER_CHECK_STATUS_BACKINDEX);
            return;
        }

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $request = $this->request->getData();
            $user = $this->Users->patchEntity($user, $request);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // ログインチェック、ユーザーid存在チェック、状況に応じてリダイレクト
        $user_id_check_res = $this->_checkUserId($id);

        if ($user_id_check_res) {
            return;
        }

        try {
            $user = $this->Users->get($id, [
                'contain' => ['Bookmarks']
            ]);
        } catch(\Exception $e) {
            $this->_exeRedirect(self::USER_CHECK_STATUS_BACKINDEX);
            return;
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            $request = $this->request->getData();

            $user = $this->Users->patchEntity($user, $request);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();

            if ($user && $user['available'] == 0) {
                $this->Flash->error(__('停止中のアカウントです。詳細は管理者にお問い合わせください。'));
                return;
            }

            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error('ユーザー名またはパスワードが不正です。');
        }
    }

    public function logout()
    {
        $this->Flash->success('ログアウトしました。');
        return $this->redirect($this->Auth->logout());
    }


    /**
     * 編集、詳細画面のパラメータidが正しいかチェックする
     * @return int クラス定数で定義したUSER_CHECK_STATUS〜を返す
     */
    public function _checkUserId($id = null)
    {
        $status = self::USER_CHECK_STATUS_OK;

        // リクエストidがnullのとき、存在しないときはアクセスできない
        if (is_null($id)) {
            $status = self::USER_CHECK_STATUS_BACKINDEX;
        }

        // admin以外の場合、リクエストidがログイン中のユーザーidと一致しないときはアクセスさせない
        if ($this->Auth->user('role') !== Configure::read('userRole.admin') &&
        $id != $this->Auth->user('id')) {
            $status = self::USER_CHECK_STATUS_BACKINDEX;
        }

        $this->_exeRedirect($status);
        return $status;
    }

    /**
     * 特定のアクションへ遷移させる
     * @param  int クラス定数で定義したUSER_CHECK_STATUS〜を使う
     * @return int $status
     */
    public function _exeRedirect($status)
    {
        
        if ($status === self::USER_CHECK_STATUS_BACKINDEX) {
            $this->Flash->error(__('URLが正しくありません。アクセス権限のないユーザーNo.です。'));
            $this->redirect(['controller' => 'Bookmarks', 'action' => 'index']);
        }

        return $status;
    }
}
