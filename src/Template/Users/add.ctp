<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */

use Cake\Core\Configure;

?>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Add User') ?></legend>
        <?php
            echo $this->Form->control('loginname', ['label' => __('ログインID'), 'value' => '']);
            echo $this->Form->control('username', ['label' => __('ニックネーム'), 'value' => '']);
            echo $this->Form->control('password', ['label' => __('パスワード'), 'value' => '']);
            echo $this->Form->control('password_confirm', ['label' => __('パスワード確認用'), 'value' => '', 'type' => 'password']);

            $role = array_combine(Configure::read('userRole'), Configure::read('userRoleView'));
            echo $this->Form->input('role', [
                'label' => __('権限'),
                'type' => 'select',
                'options' => $role,
                'default' => Configure::read('userRole.general')
            ]);

            $available = array_combine(Configure::read('available'), Configure::read('availableView'));
            echo $this->Form->input('available', [
                'label' => __('利用可能フラグ'),
                'type' => 'select',
                'options' => $available,
                'default' => Configure::read('available.true')
            ]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
