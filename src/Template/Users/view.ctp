<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */

 use Cake\Core\Configure;

?>
<div class="users view large-6 medium-8 columns content">
    <h3><?= __('ユーザー詳細') ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('ユーザーNo.') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ログインID') ?></th>
            <td><?= h($user->loginname) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ニックネーム') ?></th>
            <td><?= h($user->username) ?></td>
        </tr>
        <?php if ($auth['role'] === Configure::read('userRole.admin')) : ?>
            <tr>
                <th scope="row"><?= __('権限') ?></th>
                <td>    
                    <?= Configure::read('userRoleView')[array_flip(Configure::read('userRole'))[$this->Number->format($user->role)]]; ?>
                </td>
            </tr>
            <tr>
                <th scope="row"><?= __('利用状況') ?></th>
                <td>
                    <?= Configure::read('availableView')[array_flip(Configure::read('available'))[$this->Number->format($user->available)]]; ?>
                </td>
            </tr>
        <?php endif; ?>
        <tr>
            <th scope="row"><?= __('登録日') ?></th>
            <?php $created = new DateTime($user->created); ?>
            <td><?= h($created->format('Y-m-d H:i')); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('更新日') ?></th>
            <td>
                <?php $modified = new DateTime($user->modified); ?>
                <?= h($modified->format('Y-m-d H:i')); ?>
            </td>
        </tr>
    </table>

    <?= $this->Html->link(__('ユーザー情報編集'), ['action' => 'edit', $user->id]) ?>
</div>

<?php if ($auth['role'] === Configure::read('userRole.admin')) : ?>
    <div class="users view large-12 medium-8 columns content">
        <div class="related">
            <h4><?= __('Related Bookmarks') ?></h4>
            <?php if (!empty($user->bookmarks)): ?>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <th scope="col"><?= __('ブックマークID') ?></th>
                    <th scope="col"><?= __('ブックマーク') ?></th>
                    <th scope="col"><?= __('備考') ?></th>
                    <th scope="col"><?= __('登録日') ?></th>
                    <th scope="col"><?= __('更新日') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($user->bookmarks as $bookmarks): ?>
                <tr>
                    <td><?= h($bookmarks->id) ?></td>
                    <td><?= $this->Html->link(h($bookmarks->name), h($bookmarks->url), ['target' => '_blank']) ?></td>
                    <td><?= h($bookmarks->remarks) ?></td>
                    <?php $created = new DateTime($bookmarks->created); ?>
                    <td><?= h($created->format('Y-m-d H:i')); ?></td>
                    <?php $modified = new DateTime($bookmarks->modified); ?>
                    <td><?= h($modified->format('Y-m-d H:i')); ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('詳細'), ['controller' => 'Bookmarks', 'action' => 'view', $bookmarks->id]) ?>
                        <?= $this->Html->link(__('編集'), ['controller' => 'Bookmarks', 'action' => 'edit', $bookmarks->id]) ?>
                        <?= $this->Form->postLink(__('削除'), ['controller' => 'Bookmarks', 'action' => 'delete', $bookmarks->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bookmarks->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
