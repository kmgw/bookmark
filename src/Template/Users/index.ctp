<?php

use Cake\Core\Configure;

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<div class="users index large-12 medium-8 columns content">
    <h3><?= __('Users') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id', __('ユーザーNo.')) ?></th>
                <th scope="col"><?= $this->Paginator->sort('loginname', __('ログインID')) ?></th>
                <th scope="col"><?= $this->Paginator->sort('username', __('ニックネーム')) ?></th>
                <th scope="col"><?= $this->Paginator->sort('role', __('権限')) ?></th>
                <th scope="col"><?= $this->Paginator->sort('available', __('利用状況')) ?></th>
                <th scope="col"><?= $this->Paginator->sort('created', __('登録日')) ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified', __('更新日')) ?></th>
                <th scope="col" class="actions"><?= __('操作') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
                <td><?= $this->Number->format($user->id) ?></td>
                <td><?= h($user->loginname) ?></td>
                <td><?= h($user->username) ?></td>
                <td>
                <?= Configure::read('userRoleView')[array_flip(Configure::read('userRole'))[$this->Number->format($user->role)]]; ?>
                </td>
                <td>
                <?= Configure::read('availableView')[array_flip(Configure::read('available'))[$this->Number->format($user->available)]]; ?>
                </td>
                <td>
                <?php $created = new DateTime($user->created); ?>
                <?= h($created->format('Y-m-d H:i')); ?>
                </td>
                <td>
                <?php $modified = new DateTime($user->modified); ?>
                <?= h($modified->format('Y-m-d H:i')); ?>
                </td>
                <td class="actions">
                    <?= $this->Html->link(__('詳細'), ['action' => 'view', $user->id]) ?>
                    <?= $this->Html->link(__('編集'), ['action' => 'edit', $user->id]) ?>
                    <?= $this->Form->postLink(__('削除'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
