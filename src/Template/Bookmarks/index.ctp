<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Bookmark[]|\Cake\Collection\CollectionInterface $bookmarks
 */

use Cake\Core\Configure;

?>
<div class="bookmarks index large-12 medium-8 columns content">
    <h3><?= __('Bookmarks') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id', __('ブックマークID')) ?></th>
                <th scope="col"><?= $this->Paginator->sort('name', __('ブックマーク')) ?></th>
                <th scope="col"><?= $this->Paginator->sort('remarks', __('備考')) ?></th>
                <th scope="col"><?= $this->Paginator->sort('created', __('登録日')) ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified', __('更新日')) ?></th>
                <?php if ($auth['role'] === Configure::read('userRole.admin')) : ?>
                    <th scope="col"><?= $this->Paginator->sort('user_id', __('登録ユーザーID')) ?></th>
                <?php endif; ?>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($bookmarks as $bookmark): ?>
            <tr>
                <td><?= $this->Number->format($bookmark->id) ?></td>
                <td><?= $this->Html->link(h($bookmark->name), h($bookmark->url), ['target' => '_blank']) ?></td>
                <td><?= h($bookmark->remarks) ?></td>
                <?php $created = new DateTime($bookmark->created); ?>
                <td><?= h($created->format('Y-m-d H:i')); ?></td>
                <?php $modified = new DateTime($bookmark->modified); ?>
                <td><?= h($modified->format('Y-m-d H:i')); ?></td>
                <?php if ($auth['role'] === Configure::read('userRole.admin')) : ?>
                    <td><?= $bookmark->has('user') ? $this->Html->link($bookmark->user->id, ['controller' => 'Users', 'action' => 'view', $bookmark->user->id]) : '' ?></td>
                <?php endif; ?>
                <td class="actions">
                    <?= $this->Html->link(__('詳細'), ['action' => 'view', $bookmark->id]) ?>
                    <?= $this->Html->link(__('編集'), ['action' => 'edit', $bookmark->id]) ?>
                    <?= $this->Form->postLink(__('削除'), ['action' => 'delete', $bookmark->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bookmark->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
