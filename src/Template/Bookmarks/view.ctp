<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Bookmark $bookmark
 */

use Cake\Core\Configure;

?>
<div class="bookmarks view large-9 medium-8 columns content">
    <h3><?= h(__('ブックマーク詳細')) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('ブックマーク名') ?></th>
            <td><?= h($bookmark->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('URL') ?></th>
            <td><?= h($bookmark->url) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('備考') ?></th>
            <td><?= h($bookmark->remarks) ?></td>
        </tr>
        <?php if ($auth['role'] === Configure::read('userRole.admin')) : ?>
            <tr>
                <th scope="row"><?= __('登録ユーザーID') ?></th>
                <td><?= $bookmark->has('user') ? $this->Html->link($bookmark->user->id, ['controller' => 'Users', 'action' => 'view', $bookmark->user->id]) : '' ?></td>
            </tr>
        <?php endif; ?>
        <tr>
            <th scope="row"><?= __('ブックマークID') ?></th>
            <td><?= $this->Number->format($bookmark->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('登録日') ?></th>
            <td><?= h($bookmark->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('更新日') ?></th>
            <td><?= h($bookmark->modified) ?></td>
        </tr>
    </table>
</div>
